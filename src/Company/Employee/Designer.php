<?php

namespace Igord\KeyUaTestTask\Company\Employee;

class Designer extends BaseAbstract
{
    // ########################################

    public function __construct()
    {
        $this->addAction(new \Igord\KeyUaTestTask\Company\Action\Draw())
             ->addAction(new \Igord\KeyUaTestTask\Company\Action\CommunicateWithManager());
    }

    // ########################################
}
