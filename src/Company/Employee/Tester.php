<?php

namespace Igord\KeyUaTestTask\Company\Employee;

class Tester extends BaseAbstract
{
    // ########################################

    public function __construct()
    {
        $this->addAction(new \Igord\KeyUaTestTask\Company\Action\CodeTesting())
             ->addAction(new \Igord\KeyUaTestTask\Company\Action\CommunicateWithManager())
             ->addAction(new \Igord\KeyUaTestTask\Company\Action\AssignTask());
    }

    // ########################################
}
