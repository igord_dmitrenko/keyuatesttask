<?php

namespace Igord\KeyUaTestTask\Company\Employee;

class Programmer extends BaseAbstract
{
    // ########################################

    public function __construct()
    {
        $this->addAction(new \Igord\KeyUaTestTask\Company\Action\CodeWriting())
             ->addAction(new \Igord\KeyUaTestTask\Company\Action\CodeTesting())
             ->addAction(new \Igord\KeyUaTestTask\Company\Action\CommunicateWithManager());
    }

    // ########################################
}
