<?php

namespace Igord\KeyUaTestTask\Company\Employee;

abstract class BaseAbstract
{
    /** @var \Igord\KeyUaTestTask\Company\Action\BaseInterface[] */
    private $actions = [];

    // ########################################

    public function hasAction(\Igord\KeyUaTestTask\Company\Action\BaseInterface $action): bool
    {
        return in_array($action, $this->actions);
    }

    public function addAction(\Igord\KeyUaTestTask\Company\Action\BaseInterface $action): self
    {
        $this->actions[] = $action;

        return $this;
    }

    /**
     * @return \Igord\KeyUaTestTask\Company\Action\BaseInterface[]
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    // ########################################
}
