<?php

namespace Igord\KeyUaTestTask\Company\Employee;

class Manager extends BaseAbstract
{
    // ########################################

    public function __construct()
    {
        $this->addAction(new \Igord\KeyUaTestTask\Company\Action\AssignTask());
    }

    // ########################################
}
