<?php

namespace Igord\KeyUaTestTask\Company\Console\Command;

class EmployeeActions extends \Symfony\Component\Console\Command\Command
{
    private const NAME = 'company:employee';

    // ########################################

    protected function configure()
    {
        $this->setDescription('Show employee actions.')
             ->setName(self::NAME);

        $this->addArgument(
            'employee-name',
            \Symfony\Component\Console\Input\InputArgument::REQUIRED
        );
    }

    // ########################################

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        /** @var string $employeeName */
        $employeeName = $input->getArgument('employee-name');

        $employee = $this->findEmployee($employeeName);
        if ($employee === null) {
            $output->writeln('Employee is not found.');

            return;
        }

        $output->writeln('Actions:');
        foreach ($employee->getActions() as $action) {
            $output->writeln($action->process());
        }
    }

    // ########################################

    private function findEmployee(string $name): ?\Igord\KeyUaTestTask\Company\Employee\BaseAbstract
    {
        $class = '\Igord\KeyUaTestTask\Company\Employee\\' . ucfirst($name);
        if (!class_exists($class)) {
            return null;
        }

        return new $class();
    }

    // ########################################
}
