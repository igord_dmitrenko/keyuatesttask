<?php

namespace Igord\KeyUaTestTask\Company\Console\Command;

class EmployeeCan extends \Symfony\Component\Console\Command\Command
{
    private const NAME = 'employee:can';

    // ########################################

    protected function configure()
    {
        $this->setDescription('Check employee can do action.')
             ->setName(self::NAME);

        $this->addArgument('employee-name', \Symfony\Component\Console\Input\InputArgument::REQUIRED)
             ->addArgument('action-name', \Symfony\Component\Console\Input\InputArgument::REQUIRED);
    }

    // ########################################

    protected function execute(
        \Symfony\Component\Console\Input\InputInterface $input,
        \Symfony\Component\Console\Output\OutputInterface $output
    ) {
        /** @var string $employeeName */
        /** @var string $actionName */
        $employeeName = $input->getArgument('employee-name');
        $actionName   = $input->getArgument('action-name');

        $employee = $this->findEmployee($employeeName);
        if ($employee === null) {
            $output->writeln('Employee is not found.');

            return;
        }

        $action = $this->findAction($actionName);
        if ($action === null) {
            $output->writeln('Action is not found.');

            return;
        }

        if ($employee->hasAction($action)) {
            $output->writeln('true');

            return;
        }

        $output->writeln('false');
    }

    // ########################################

    private function findEmployee(string $name): ?\Igord\KeyUaTestTask\Company\Employee\BaseAbstract
    {
        $class = '\Igord\KeyUaTestTask\Company\Employee\\' . ucfirst($name);
        if (!class_exists($class)) {
            return null;
        }

        return new $class();
    }

    private function findAction(string $name): ?\Igord\KeyUaTestTask\Company\Action\BaseInterface
    {
        $class = '\Igord\KeyUaTestTask\Company\Action\\' . ucfirst($name);
        if (!class_exists($class)) {
            return null;
        }

        return new $class();
    }

    // ########################################
}
