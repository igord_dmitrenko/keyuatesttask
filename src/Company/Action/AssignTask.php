<?php

namespace Igord\KeyUaTestTask\Company\Action;

class AssignTask implements BaseInterface
{
    // ########################################

    public function process(): string
    {
        return 'assign task';
    }

    // ########################################
}
