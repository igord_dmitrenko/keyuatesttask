<?php

namespace Igord\KeyUaTestTask\Company\Action;

class Draw implements BaseInterface
{
    // ########################################

    public function process(): string
    {
        return 'draw';
    }

    // ########################################
}
