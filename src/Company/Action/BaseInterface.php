<?php

namespace Igord\KeyUaTestTask\Company\Action;

interface BaseInterface
{
    // ########################################

    public function process(): string;

    // ########################################
}
