<?php

namespace Igord\KeyUaTestTask\Company\Action;

class CodeTesting implements BaseInterface
{
    // ########################################

    public function process(): string
    {
        return 'code testing';
    }

    // ########################################
}
