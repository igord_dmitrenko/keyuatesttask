<?php

namespace Igord\KeyUaTestTask\Company\Action;

class CodeWriting implements BaseInterface
{
    // ########################################

    public function process(): string
    {
        return 'code writing';
    }

    // ########################################
}
