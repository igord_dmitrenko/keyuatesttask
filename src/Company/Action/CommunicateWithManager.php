<?php

namespace Igord\KeyUaTestTask\Company\Action;

class CommunicateWithManager implements BaseInterface
{
    // ########################################

    public function process(): string
    {
        return 'communicate with manager';
    }

    // ########################################
}
