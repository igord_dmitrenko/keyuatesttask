<?php

require __DIR__ . '/../vendor/autoload.php';

$application = new \Symfony\Component\Console\Application();
$application->addCommands(
    [
        new \Igord\KeyUaTestTask\Company\Console\Command\EmployeeCan(),
        new \Igord\KeyUaTestTask\Company\Console\Command\EmployeeActions(),
    ]
);

$application->run();
