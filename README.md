For install run: composer install

---------------------
---------------------

Console commands:

---

Show allowed actions: 
Return: list of allowed actions

php bin/console.php company:employee {employee-name}

Allowed employee names:
- programmer
- designer
- manager
- tester

Examples:
php bin/console.php company:employee programmer
php bin/console.php company:employee designer
php bin/console.php company:employee manager
php bin/console.php company:employee tester

---

Check employee can do some action:
Return: true|false

php bin/console.php employee:can {employee-name} {action-name}

Allowed employee names:
- programmer
- designer
- manager
- tester

Allowed action names:
- assignTask
- codeTesting
- codeWriting
- draw
- communicateWithManager

Examples:
php bin/console.php employee:can programmer codeTesting

---------------------
---------------------

MySQL test task in mysql_test_task directory.
