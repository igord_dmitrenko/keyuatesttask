/**
  Для заданного списка товаров получить названия всех категорий, в которых представленытовары.
  Выборка для нескольких товаров (пример: ids = (9, 14, 6, 7, 2) ).
 */

SELECT DISTINCT `name` AS `category_name`
FROM Category
         JOIN Product_Category
              ON Product_Category.category_id = Category.id
WHERE Product_Category.product_id IN (5, 6, 7, 8);

/**
  Для заданной категории получить список предложений всех товаров из этой категории.
  Каждая категория может иметь несколько подкатегорий.
  Пример:
  Выбираю все товары из категории компьютеры (id = 2) и подкатегории (id =3 (ноутбуки), id = 4 (планшеты), id = 5(гибриды)).
 */

SELECT DISTINCT `name` AS `product_name`
FROM Product
         JOIN Product_Category ON Product_Category.product_id = Product.id
WHERE Product_Category.category_id IN (
    SELECT id
    FROM Category
    WHERE id = 1
    UNION
    SELECT id
    FROM Category
    WHERE parent_id = 1
);

/**
   Для заданного списка категорий получить количество уникальных товаров в каждой категории.
   Выборка для нескольких категорий (пример: ids = (2, 3, 4) ).
 */

SELECT `name` AS `category_name`, COUNT(DISTINCT Product_Category.product_id) as `products_number`
FROM Category
         JOIN Product_Category ON Product_Category.category_id = Category.id
WHERE Product_Category.category_id IN (10, 17)
GROUP BY category_name;


/**
    Для заданного списка категорий получить количество единиц каждого товара который
    входит в указанные категории.
    Выборка для нескольких категорий (пример: ids = (3, 4, 5) ).
 */
SELECT `name` as `category_name`, COUNT(Product_Category.product_id) as `products_number`
FROM Category
         JOIN Product_Category ON Product_Category.category_id = Category.id
WHERE Product_Category.category_id IN (10, 17)
GROUP BY category_name;
